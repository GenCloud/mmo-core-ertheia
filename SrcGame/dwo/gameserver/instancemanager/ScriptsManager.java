package dwo.gameserver.instancemanager;

import dwo.gameserver.model.world.quest.Quest;
import javolution.util.FastSet;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

public class ScriptsManager
{
	private static final Logger _log = LogManager.getLogger(ScriptsManager.class);

	// Пеккеджы скриптов в ядре (при добавлении нового пеккеджа в скрипты ядра ОБЯЗАТЕЛЬНО вписать его сюда)
	// TODO: Автоматическое изъятие пеккеджей из SCRIPT_PATH
	private static final String SCRIPT_PATH = "dwo.scripts.";
	private static final String[] SCRIPT_PKGS = {
		"ai", "conquerablehalls", "custom", "dynamicspawn", "events", "hellbound", "industrials", "instances", "npc",
		"quests", "services", "vehicles", "dynamic_quests"
	};

	private ScriptsManager()
	{
		_log.log(Level.INFO, "Initializing Script Engine Manager");
	}

	public static ScriptsManager getInstance()
	{
		return SingletonHolder._instance;
	}

	private static void addScript(Collection<Class<?>> classes, String name)
	{
		try
		{
			Class<?> cl = Class.forName(name);
			if(cl != null && Quest.class.isAssignableFrom(cl))
			{
				classes.add(cl);
			}
		}
		catch(Exception e)
		{
			_log.log(Level.ERROR, e.getMessage(), e);
		}
	}

	private static Collection<Class<?>> getClassesForPackageInDir(File directory, String packageName, Collection<Class<?>> classes)
	{
		if(!directory.exists())
		{
			return classes;
		}
		File[] files = directory.listFiles();
        if (files != null) {
            for(File file : files)
            {
                if(file.isDirectory())
                {
                    getClassesForPackageInDir(file, packageName + '.' + file.getName(), classes);
                }
                else if(file.getName().endsWith(".class"))
                {
                    addScript(classes, packageName + '.' + file.getName().substring(0, file.getName().length() - 6));
                }
            }
        }
        return classes;
	}

	public void executeCoreScripts()
	{
		for(String pkg : SCRIPT_PKGS)
		{
			Collection<Class<?>> classes = getClassesForPackage(SCRIPT_PATH + pkg);
			for(Class<?> cls : classes)
			{
				if(cls.getSimpleName().equals("AirShipController"))
				{
					continue;
				}
				try
				{
					Method m = cls.getMethod("main", new Class[]{String[].class});
					if(m.getDeclaringClass().equals(cls)) // Check for classes like Sagas
					{
						m.invoke(cls, new Object[]{new String[]{}});
					}
					continue;
				}
				catch(NoSuchMethodException e)
				{
                    _log.error("", e);
				}
				catch(InvocationTargetException | IllegalAccessException e)
				{
					_log.log(Level.ERROR, e.getMessage(), e);
				}
				try
				{
					Constructor<?> c = cls.getConstructor(new Class[]{});
					Quest q = (Quest) c.newInstance();
					q.setAltMethodCall(true);
				}
				catch(NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e)
				{
					_log.log(Level.ERROR, e.getMessage(), e);
				}
			}
		}
	}

	private void getClassesForPackageInJar(URL url, String packagePath, Collection<Class<?>> classes)
	{
		JarInputStream stream;
		try
		{
			stream = new JarInputStream(url.openStream()); // may want better way to open url connections
			JarEntry entry = stream.getNextJarEntry();
			while(entry != null)
			{
				String name = entry.getName();
				int i = name.lastIndexOf('/');
				if(i > 0 && name.endsWith(".class") && name.substring(0, i).startsWith(packagePath))
				{
					addScript(classes, name.substring(0, name.length() - 6).replace("/", "."));
				}
				entry = stream.getNextJarEntry();
			}
			stream.close();
		}
		catch(IOException e)
		{
			_log.log(Level.ERROR, "Can't get classes for url " + url + ": " + e.getMessage());
		}
	}

	public Collection<Class<?>> getClassesForPackage(String packageName)
	{
		String packagePath = packageName.replace(".", "/");
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		Collection<Class<?>> classes = new FastSet<>();
		try
		{
			Enumeration<URL> resources = classLoader.getResources(packagePath);
			List<File> dirs = new ArrayList<>();
			while(resources.hasMoreElements())
			{
				URL resource = resources.nextElement();
				dirs.add(new File(resource.getFile()));
			}
			for(File directory : dirs)
			{
				getClassesForPackageInDir(directory, packageName, classes);
			}
		}
		catch(IOException e)
		{
			_log.log(Level.ERROR, e.getMessage(), e);
		}
		List<URL> jarUrls = new ArrayList<>();
		while(classLoader != null)
		{
			if(classLoader instanceof URLClassLoader)
			{
				for(URL url : ((URLClassLoader) classLoader).getURLs())
				{
					if(url.getFile().endsWith("libs/game.jar"))
					{
						jarUrls.add(url);
					}
				}
			}
			classLoader = classLoader.getParent();
		}
		for(URL url : jarUrls)
		{
			getClassesForPackageInJar(url, packagePath, classes);
		}
		return classes;
	}

	private static class SingletonHolder
	{
		protected static final ScriptsManager _instance = new ScriptsManager();
	}
}
